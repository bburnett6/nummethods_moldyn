from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt 
import numpy as np 
import sys

def main():
	if len(sys.argv) != 3:
		print('python plot_nbody.py method n_p')
		sys.exit()
	method = sys.argv[1]
	n_p = int(sys.argv[2])
	ks = []
	vs = []
	es = []
	ts = []
	with open(f'{method}_energy', 'r') as f:
		for line in f.readlines():
			ls = line.split()
			es.append(float(ls[-1]))
			vs.append(float(ls[-2]))
			ks.append(float(ls[-3]))
			ts.append(float(ls[0]))

	xs = [[] for n in range(n_p)]
	ys = [[] for n in range(n_p)]
	zs = [[] for n in range(n_p)] 
	with open(f'{method}_trajectories', 'r') as f:
		for i, line in enumerate(f.readlines()):
			n = i % n_p
			ls = line.split()
			xs[n].append(float(ls[0]))
			ys[n].append(float(ls[1]))
			zs[n].append(float(ls[2]))

	fig = plt.figure()
	ax = fig.gca(projection='3d')
	#ax.plot3D(xs, ys, zs)
	cs = ['red', 'green', 'blue', 'yellow', 'orange', 'purple']
	for i in range(n_p):
		ax.scatter(xs[i], ys[i], zs[i], c=cs[i])
	#ax.set_xlim(-1.3, 1.3)
	#ax.set_ylim(-1.3, 1.3)
	plt.grid()
	plt.show()

	plt.figure()
	plt.plot(ts, es, label='E')
	plt.plot(ts, ks, label='K')
	plt.plot(ts, vs, label='V')
	#plt.ylim([-3, 3])
	#plt.plot(np.array(ts), np.array(ks) + np.array(vs), label='K+V') #this checks out
	plt.title(f'{method} energies')
	plt.legend()
	#plt.savefig(f'{method}_e_dt01.png')
	plt.show()

if __name__ == '__main__':
	main()