import numpy as np 

#Sources:
#MD
#https://dasher.wustl.edu/chem430/lectures/lecture-05.pdf
#https://www.physics.drexel.edu/~valliere/PHYS305/MolecularDynamics/MolecularDynamics/MolecularDynamics.html
#Allen-Tildesley text

#Time steppers
#https://www.brown.edu/research/projects/scientific-computing/sites/brown.edu.research.projects.scientific-computing/files/uploads/Strong Stability-Preserving High-Order Time Discretization Methods.pdf
#https://gkyl.readthedocs.io/en/latest/dev/ssp-rk.html
#http://spiff.rit.edu/richmond/nbody/OrbitRungeKutta4.pdf


class MolecularDynamicsSim(object):
	def __init__(self, num_parts=2, dt=0.0001, boxsize=20, periodicBC=True):
		self.n = num_parts 
		self.dt = dt 
		self.tc = 0.0 #current time

		#Boundary conditions
		#boxsize is 2 * L so for boxsize=20 the coodinates of the box are [-10, 10]
		self.boxsize = boxsize 
		self.doPeriodicBC = periodicBC 

		###############################################
		# Particle Position, velocity, and acceleration
		###############################################
		#self.parts_rn = np.random.rand(num_parts, 3)
		#self.parts_rn = np.array([[0, 0, 0], [1.2, 0, 0], [0, 1.2, 0]])
		#Positions initialized with random (uniform) value on [-L/2, L/2]
		self.parts_rn = (np.random.rand(num_parts, 3) - 0.5) * boxsize / 4
		#Velocities initialized with random (uniform) value on [-1, 1]
		self.parts_vn = (np.random.rand(num_parts, 3) - 0.5) * 2.0
		self.parts_an = np.zeros((num_parts, 3))

		#################
		# Particle mass
		#################
		#Currently set to 1
		#self.parts_m = np.random.rand(num_parts, 1)
		self.parts_m = np.ones((num_parts, 1))

		#Modify velocity to remove CM motion
		"""
		p = np.zeros(3)
		tot_m = np.sum(self.parts_m)
		for i in range(self.n):
			p[0] = p[0] + self.parts_m[i] * self.parts_vn[i][0]
			p[1] = p[1] + self.parts_m[i] * self.parts_vn[i][1]
			p[2] = p[2] + self.parts_m[i] * self.parts_vn[i][2]
		for i in range(self.n):
			self.parts_vn[i][0] = self.parts_vn[i][0] - p[0] / tot_m
			self.parts_vn[i][1] = self.parts_vn[i][1] - p[1] / tot_m
			self.parts_vn[i][2] = self.parts_vn[i][2] - p[2] / tot_m
		"""
		#####################################################
		# particle next and previous steps for time stepping
		#####################################################
		self.parts_rnp1 = self.parts_rn.copy()
		self.parts_vnp1 = self.parts_vn.copy()
		self.parts_anp1 = self.parts_an.copy()
		self.parts_anm1 = self.parts_an.copy() #for beeman algorithm
		
		##################
		# Particle Energy
		##################
		self.K = 0.
		self.V = 0.
		self.calc_energy()

	###########################
	# Force/Energy Functions
	###########################

	"""
	Calculate the force based on the Lennard-Jones 12-6 Potential
	"""
	def calc_force(self):
		#see https://www.physics.drexel.edu/~valliere/PHYS305/MolecularDynamics/MolecularDynamics/MolecularDynamics.html
		#epsilon = 1.0 #using normalized constant for permitivity of free space
		forces = np.zeros((self.n, 3))
		for i, a in enumerate(self.parts_rn):
			for j in range(i+1, self.n):
				b = self.parts_rn[j]
				rij = a - b 
				#force is the negative derivative of the Lennard-Jones Potential 
				# fx = sum{ 4e(12/|rij|^14 - 6/|rij|^8) * xij } where e is permitivity of free space
				rij_n = np.linalg.norm(rij)
				lj_factor =  4.0 * (12. / (rij_n**14) - 6. / (rij_n**8))
				fij = lj_factor * rij
				forces[i] += fij
				forces[j] -= fij
		
		#Debug
		"""
		ftot = np.zeros(3)
		for i in range(self.n):
			print(f'p{i} {forces[i][0]} {forces[i][1]} {forces[i][2]}')
			ftot += forces[i]
		print(f'{ftot}') #this should be zero
		"""

		#note the negative in f = -dV/dr has been accounted for and a = f/m
		return forces
	
	"""
	Calculate the energy based on the Lennard-Jones 12-6 Potential
	"""
	def calc_energy(self):
		#epsilon = 1.0
		#Potential energy
		potential = 0.
		for i, a in enumerate(self.parts_rn):
			for j in range(i+1, self.n):
				b = self.parts_rn[j]
				rij = a - b 
				rij = rij - self.boxsize * np.floor(rij / self.boxsize + 0.5)

				rij_n = np.linalg.norm(rij)
				potential += 4 * (1. / rij_n**12 - 1. / rij_n**6)
		self.V = potential 

		#Kinetic energy 
		kinetic = 0.
		for i in range(self.n):
			kinetic += 0.5 * self.parts_m[i][0] * np.linalg.norm(self.parts_vn[i])**2
		self.K = kinetic 

	#################################
	# Boundary Condition Functions
	#################################

	def periodic_bc(self):
		#numpy magic: multiply a boolean array by scalar array. If false the element is
		#multiplied to zero if true multiplied to value

		upper_test = np.ones(3) * self.boxsize / 2.
		self.parts_rn -= np.greater(self.parts_rn, upper_test) * np.ones(3) * self.boxsize

		lower_test = np.ones(3) * -self.boxsize / 2
		self.parts_rn += np.less(self.parts_rn, lower_test) * np.ones(3) * self.boxsize

	##########################
	# Update Functions
	##########################
	def update_verlet(self):
		#velocity verlet algorithm eqs 3.19a and 3.19b from Allen-Tildesly text
		self.parts_rnp1 = self.parts_rn + self.parts_vn * self.dt + self.parts_an * self.dt**2 / 2.0
		self.parts_rn = self.parts_rnp1.copy()
		forces = self.calc_force()

		self.parts_anp1 = forces / self.parts_m 
		self.parts_vnp1 = self.parts_vn + (self.parts_an + self.parts_anp1) * self.dt / 2.0
 
		self.parts_vn = self.parts_vnp1.copy()
		self.parts_an = self.parts_anp1.copy()

		self.tc += self.dt 
		if self.doPeriodicBC:
			self.periodic_bc()

	def update_beeman(self):
		#beeman algorithm eqs 3.22a and 3.22b from Allen-Tildesly text
		#There potentially needs to be an initialization through another method
		#anm1 is modified in this method and starts as a copy of an
		#Might need to modify this to account for that either by adding an anm1
		#statement to verlet and calling that for the first time step or something else
		self.parts_rnp1 = self.parts_rn + self.parts_vn * self.dt + self.parts_an * self.dt**2 * 2./3. - self.parts_anm1 * self.dt**2 / 6.
		self.parts_rn = self.parts_rnp1.copy()
		forces = self.calc_force()

		self.parts_anp1 = forces / self.parts_m 		
		self.parts_vnp1 = self.parts_vn + self.parts_anp1 * self.dt / 3. + self.parts_an * self.dt * 5./6. - self.parts_anm1 * self.dt / 6.

		self.parts_anm1 = self.parts_an.copy() 
		self.parts_vn = self.parts_vnp1.copy()
		self.parts_an = self.parts_anp1.copy()

		self.tc += self.dt 
		if self.doPeriodicBC:
			self.periodic_bc()

	def update_rk4(self):
		# See http://spiff.rit.edu/richmond/nbody/OrbitRungeKutta4.pdf
		forces = self.calc_force()

		k0r = self.parts_vn
		k0v = forces / self.parts_m 

		real_r = self.parts_rn.copy()
		self.parts_rn = self.parts_rn + self.dt * k0r / 2 

		forces = self.calc_force()

		k1r = self.parts_vn + self.dt * k0v / 2
		k1v = forces / self.parts_m

		self.parts_rn = real_r + self.dt * k1r / 2
		forces = self.calc_force()

		k2r = self.parts_vn + self.dt * k1v / 2
		k2v = forces / self.parts_m 

		self.parts_rn = real_r + self.dt * k2r
		forces = self.calc_force()

		k3r = self.parts_vn + self.dt * k2v
		k3v = forces / self.parts_m 

		self.parts_rnp1 = real_r + self.dt / 6. * (k0r + 2 * k1r + 2 * k2r + k3r)
		self.parts_vnp1 = self.parts_vn + self.dt / 6. * (k0v + 2 * k1v + 2 * k2v + k3v)
		self.parts_anp1 = k3v

		self.parts_rn = self.parts_rnp1
		self.parts_vn = self.parts_vnp1
		self.parts_an = self.parts_anp1

		self.tc += self.dt 
		if self.doPeriodicBC:
			self.periodic_bc()

	def update_ssp_rk3(self):
		# See https://gkyl.readthedocs.io/en/latest/dev/ssp-rk.html
		forces = self.calc_force()
		real_r = self.parts_rn.copy()
		self.parts_anp1 = forces / self.parts_m

		k0r = real_r + self.dt * self.parts_vn 
		k0v = self.parts_vn + self.dt * forces / self.parts_m

		self.parts_rn = k0r 
		forces = self.calc_force()

		k1r = 0.75 * real_r + 0.25 * (k0r + self.dt * k0v)
		k1v = 0.75 * self.parts_vn + 0.25 * (k0v + self.dt * forces / self.parts_m)

		self.parts_rn = k1r
		forces = self.calc_force()

		self.parts_rnp1 = 1. / 3. * real_r + 2. / 3. * (k1r + self.dt * k0v)
		self.parts_vnp1 = 1. / 3. * self.parts_vn + 2. / 3. * (k1v + self.dt * forces / self.parts_m)

		self.parts_rn = self.parts_rnp1
		self.parts_vn = self.parts_vnp1
		self.parts_an = self.parts_anp1

		self.tc += self.dt 
		if self.doPeriodicBC:
			self.periodic_bc()

	def update_ssp_4stage_rk3(self):
		# See https://gkyl.readthedocs.io/en/latest/dev/ssp-rk.html
		forces = self.calc_force()
		real_r = self.parts_rn.copy()
		self.parts_anp1 = -forces / self.parts_m 

		k0r = 0.5 * real_r + 0.5 * (real_r + self.dt * self.parts_vn)
		k0v = 0.5 * self.parts_vn + 0.5 * (self.parts_vn + self.dt * forces / self.parts_m)

		self.parts_rn = k0r
		forces = self.calc_force()

		k1r = 0.5 * k0r + 0.5 * (k0r + self.dt * self.parts_vn)
		k1v = 0.5 * k0v + 0.5 * (k0v + self.dt * forces / self.parts_m)

		self.parts_rn = k1r
		forces = self.calc_force()

		k2r = 2. / 3. * real_r + 1. / 6. * k1r + 1. / 6. * (k1r + self.dt * self.parts_vn)
		k2v = 2. / 3. * self.parts_vn + 1. / 6. * k1v + 1. / 6. * (k1v + self.dt * forces / self.parts_m)

		self.parts_rn = k2r
		forces = self.calc_force()

		self.parts_rnp1 = 0.5 * k2r + 0.5 * (k2r + self.dt * self.parts_vn)
		self.parts_vnp1 = 0.5 * k2v + 0.5 * (k2v + self.dt * forces / self.parts_m)

		self.parts_rn = self.parts_rnp1
		self.parts_vn = self.parts_vnp1
		self.parts_an = self.parts_anp1

		self.tc += self.dt 
		if self.doPeriodicBC:
			self.periodic_bc()

	#######################
	# Run logic function
	#######################
	def run(self, tstep_method, record_every, tmax):
		tr_fname = f'{tstep_method}_trajectories'
		en_fname = f'{tstep_method}_energy'
		nts = int(tmax / self.dt)
		with open(tr_fname, 'w') as tr_f, open(en_fname, 'w') as en_f:
			for t in range(nts):
				if t % record_every == 0: 
					for i in range(self.n):  #record all particles together in the same file
						tr_f.write(f'{self.parts_rn[i, 0]} {self.parts_rn[i, 1]} {self.parts_rn[i, 2]}\n')
					self.calc_energy()
					en_f.write(f'{self.tc} {self.V} {self.K} {self.V + self.K}\n')

				if tstep_method == 'verlet':
					self.update_verlet()
				elif tstep_method == 'beeman':
					self.update_beeman()
				elif tstep_method == 'rk4':
					self.update_rk4()
				elif tstep_method == 'ssp_rk3':
					self.update_ssp_rk3()
				elif tstep_method == 'ssp_4stage_rk3':
					self.update_ssp_4stage_rk3()
				

########
# Main
########

def main():
	np.random.seed(12345)
	n_p = 5
	dt = 0.0001 # 1 femtosecond (with respect to picosecond)
	box = 10.0
	sim = MolecularDynamicsSim(num_parts=n_p, dt=dt, boxsize=box)
	tmax = 20.0 # 10 pico seconds
	record_every = 100 #only record every 3 timesteps
	
	sim.run('verlet', record_every, tmax)


if __name__ == '__main__':
	main()
